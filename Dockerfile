FROM fedora:37

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake:/home/tango/lib64/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib64/pkgconfig

RUN dnf -y update &&  \
  dnf -y install      \
  abseil-cpp-devel    \
  automake            \
  cmake               \
  cppzmq-devel        \
  curl                \
  gcc                 \
  gcc-c++             \
  git                 \
  grpc-devel          \
  libjpeg-turbo-devel \
  libcurl-devel       \
  make                \
  omniORB-devel       \
  patch               \
  pkgconf-pkg-config  \
  protobuf-devel      \
  python3             \
  sudo                \
  zeromq-devel &&     \
  dnf clean all

RUN groupadd -g "$APP_GID" tango                             && \
  useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango && \
  usermod -a -G wheel tango                                  && \
  echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers       && \
  echo "/home/tango/lib" > /etc/ld.so.conf.d/home.conf

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh          && \
    ./install_tango_idl.sh     && \
    ./install_catch.sh         && \
    ./install_opentelemetry.sh && \
    rm -rf dependencies
